package ru.t1.nikitushkina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    void clear();

    @NotNull
    Project create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@Nullable String userId, @NotNull String name);

}
